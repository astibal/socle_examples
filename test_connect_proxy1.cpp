/*
    Socle - Socket Library Ecosystem
    Copyright (c) 2014, Ales Stibal <astib@mag0.net>, All rights reserved.

    This library  is free  software;  you can redistribute  it and/or
    modify  it  under   the  terms of the  GNU Lesser  General Public
    License  as published by  the   Free Software Foundation;  either
    version 3.0 of the License, or (at your option) any later version.
    This library is  distributed  in the hope that  it will be useful,
    but WITHOUT ANY WARRANTY;  without  even  the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    
    See the GNU Lesser General Public License for more details.
    
    You  should have received a copy of the GNU Lesser General Public
    License along with this library.
*/

/*
    This is the example, counterpart file for Socle library.
    Code is simpliefied and for demonstration purposes only. 
 */

#define SIGSLOT_USE_POSIX_THREADS
#include <sigslot.h>       // for signals

#include <baseproxy.hpp>   // for baseProxy class

#include <tcpcom.hpp>      // for TCP com class

#include <display.hpp>     // for hex_dump

#include <logger.hpp>      // for logging setup


// Socle has been developed to handle various proxying scenarios. 
// This means basically socket management, and relations between sockets.

// There are three conceptual classes, which handle different tasks.

// Proxy - it has two sides. left, and right. Usually you add one socket to 
//         left, and one to right (it's bit more complicated, see CX later). 
//         Simplest implementation of proxy between two connections would 
//         imply copying bytes from left to right, and vice versa.

// Com   - Com object understands how to send and receive data. Each Proxy
//         and CX (more on that later) has to have its Com object associated.
//         This ensures data to be sent correctly.
//         We have different classes for TCP, or UDP com, for the example.

// CX    - Abbreviation of "context". CX is the abstraction for socket.
//         CX is added to proxy, to either left, or to the right side.


// Let's define template handling preparation of proxy with only one CX (on the left side).
// You can easily write your class directly, but this saves quite lot of time 
// and keeps good abstraction.

// We also use Sigslot library. This is because we would need couple of more classes
// to handle data, etc. This would keep the example simple and clean.

template <class COM, class CX, class PX>
class SimpleClient : public sigslot::has_slots<sigslot::multi_threaded_local> {
    public:
        
        // Empty constructor. Just create the object and don't mess with variables.
        SimpleClient<COM,CX,PX>() {}; 
        
        // Initialize objects
        SimpleClient<COM,CX,PX>(const char* h, const char* p)  {
        
            // create proxy, based on PX template parameter, with COM parameter for Com object.
            px_ = new PX(new COM());
            
            // make proxy root. This means if you issue run() on it, it will poll for I/O.
            // I/O is handled for all slave objects.
            px_->pollroot(true);

            // initialize CX and make its COM slave of proxy COM (it will be polled by it).
            cx_ = new CX(px_->com()->slave(),h,p);
        };
        
        // delete proxy. It will also delete all CX, if they still exist.
        virtual ~SimpleClient() {
            delete px_;
        }
        
        // utility getter
        inline CX* cx() { return cx_; }
        inline PX* px() { return px_; }
        
        
        // connect. Connect is performed on CX level. Then, if successful, add CX to proxy.
        virtual int connect(bool b=false) { 
            int r = cx()->connect(b); 
            if(r > 0) px()->ladd(cx()); 
            return r; 
        }
        
        // run proxy's run
        virtual int run() { return px()->run(); }
    protected:
        PX* px_ = nullptr;
        CX* cx_ = nullptr;
};


// now we need to create some really dummy proxy. It won't do much, just handle IO and die on error.
class SimplePX : public baseProxy {
    public:
        SimplePX(baseCom* c) : baseProxy(c) {};
        
        // die on left or right error.
        virtual void on_left_error(baseHostCX*) {  dead(true); };
        virtual void on_right_error(baseHostCX*) { dead(true); };
        
        virtual void on_left_bytes(baseHostCX* cx) {
            std::cerr << "== received data" << std::endl;
            std::cout << hex_dump(cx->readbuf()) << std::endl;
            
            //demo. that's it.
            dead(true);
        }
};


int main(int argc, const char* argv[]) {
    
    std::cerr << "==\n== This is demo TCP client using socle library.\n== Program reads data received from server and exits.\n==" << std::endl;
    
    if(argc != 3) { 
        std::cerr << "\nUsage: \n" << argv[0] << " HOST PORT\n" << std::endl;
        return -1;
    }

    
    get_logger()->level(NON);      // get access to socle logger and set its logging level
                                   // socle does quite extensive logging, if asked -- logging levels are NON,FAT,CRI,ERR,WAR,NOT,INF,DIA,DEB,DUM,EXT
                                   // from none ... to extreme. DIA suits most troubleshooting needs, DEB if you really need more info. DUM and EXT are intensive.
                                   // INF is for INFormational messages. It's the default, and by default library won't log anything.
    
                                   // TEST DIA level, and see what you can see.
    
    get_logger()->dup2_cout(true); // Socle is not designed for logging to stdout. It expects some file logging, etc. To specifically instruct Socle to log to
                                   // standard output, use dup2_cout.


    // initialize our Simple client. 
    // use baseHostCX, for CX. For normal I/O without any special magic it's perfectly OK.
    // SimplePX manages proxying. It marks proxy dead if we receive any data. This is why you shoud test it on something returning data, i.e. SMTP.
    
    SimpleClient<TCPCom,baseHostCX,SimplePX> client(argv[1],argv[2]);
    if( client.connect() > 0) {  
        
        std::cerr << "== connected" << std::endl;
        client.run();
    }

    // print out some simple stats
    std::cerr << "== total bytes: " << client.cx()->meter_read_bytes << std::endl;
    
    return 0;
}
