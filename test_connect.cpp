#include <logger.hpp>
#include <hostcx.hpp>
#include <time.h>

#include <tcpcom.hpp>

/*
 * Non-blocking HostCX extra-basic example to get the idea.
 * 
 * Reads from socket and prints out incoming bytes, until peer closes the connection, or 10 seconds elapsed (whichever comes first).
 * Warning: this is just a demonstration: things are a bit over-simplified!
 */

// inherit from baseHostCX, use TCPCom communication layer
class MyHostCX : public baseHostCX {
public:
	
	// surprise, constructor filling hostname and port
	MyHostCX( const char* h, const char* p ) : baseHostCX::baseHostCX(new TCPCom(),h,p) {};
	MyHostCX( int s ) : baseHostCX::baseHostCX(new TCPCom(),s) {};
	
	// first useful code: overriding process() method: do something with read buffer when bytes arrived into the socket
    virtual int process() {
		
		// IMPORTANT: where are those incoming data? In the readbuf() !!!
		unsigned char *ptr = readbuf()->data();
		unsigned int len = readbuf()->size();
		
		// our only processing: hex dup the payload to the log
		INFS_("Incoming data:\n"+hex_dump(ptr,len));
		
		// IMPORTANT: with returning len, read buffer will be truncated by 'len' bytes. Note: truncated bytes are LOST.
		return len;
	};
};

int main(int argc, char *argv[]) {
	
	// we will clear session after 10s 
	const unsigned int timeout = 10;
	
	// setting logging facility level
	lout.level(INF);
	
	// some idiot-proof help
	if (argc != 3) {
		ERR_("Usage: %s <hostname> <port>",argv[0]);
		return -1;
	}
	
	// adding meat to bones: construct host context (MyHostCX) object!
	auto hcx = MyHostCX(argv[1],argv[2]);
	
	// now connect to it - NON-blocking
	hcx.connect();

	// some timer variable
	time_t t;
	time(&t);
	

	// quick and dirty read/write loop : don't do that in real world
	// if there is no error in the context, just loop
	while(!hcx.error()) {
		
		// read from socket: baseHostCX will use read function from Com template, for us it's TCPCom
		// read() will also trigger process() at the right point.
		int r = hcx.read();

		// check if socket is not ready, in such a case sleep for a while
		if (r < 0) {
			//INF_("sleep: %s",strerror(r));
			usleep(10000); //sleep for 10ms if nothing read, just to improve it a bit 
		}
		
		// timer trivia
		time_t now;
		time(&now);
		
		// break if timeout reached
		if (now - t > 10) {
			INF_("Closing connection, timeout %ds.",timeout);
			break;
		}
	}
	
	// don't forget to close the context
	hcx.close();
	
	return 0;
}
