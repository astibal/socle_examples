#include <iostream>
#include <signature.hpp>
#include <ranges.hpp>

int main(int argc, char *argv[]) {
    std::cout << "Pattern test\n";
    
    simpleMatch b("car");
    
    range x = b.match("russian cars suck!\n",0);
    std::cout << rangetos(x) << "\n";
    std::cout << rangetos(b()) << "\n";
    std::cout << rangetos(b("eicar is not virus!",0)) << "\n";
    std::cout << rangetos(b("eicar is not virus, eicar is a testing virus pattern, despite Russian car doesn't like it!",0)) << "\n";

    std::cout << "---\n";
    std::cout << rangetos(b("eicar is not virus,\r\n eicar is a testing virus pattern,\r\n despite Russian car doesn't like it!",0)) << "\n";


    
    typedef Flow<unsigned char> LRFlow;
    typedef flowMatch<unsigned char> LRSimpleFlowMatch;
    
    LRFlow f;
    f.append('l',"abc",3);
    f.append('l',"def",3);
    f.append('r',"123",3);
    
    LRSimpleFlowMatch s;
    s.add('l',new simpleMatch("ab"));
    s.add('r',new simpleMatch("1"));
    s.add('l',new simpleMatch("fgdf"));
    
    vector_range ret = s.match(&f);
    std::cout << "---\n";
    for(unsigned int i = 0; i < ret.size() ; i++)
        std::cout << i << ": " << rangetos(ret[i]) << "\n";
}