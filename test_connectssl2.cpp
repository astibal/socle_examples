#include <logger.hpp>
#include <hostcx.hpp>
#include <baseproxy.hpp>
#include <masterproxy.hpp>
#include <csignal>
#include <sslcom.hpp>

/*
 * Simple example of interconnecting two sockets using baseProxy library class
 */


// STOLEN from test_connect.cpp - when processing bytes from readbuf, hex-print all data
// inherit from baseHostCX, use TCPCom communication layer

// NEW: STOLEN from test_connect2, just redo it to use SSLCom

class MyHostCX : public baseHostCX {
public:
	
	// surprise, constructor filling hostname and port
	MyHostCX( const char* h, const char* p ) : baseHostCX::baseHostCX(new SSLCom(),h,p) {};
	MyHostCX( int s ) : baseHostCX::baseHostCX(new SSLCom(),s) {};
	
	// first useful code: overriding process() method: do something with read buffer when bytes arrived into the socket
    virtual int process() {
		
		// IMPORTANT: where are those incoming data? In the readbuf() !!!
		unsigned char *ptr = readbuf()->data();
		unsigned int len = readbuf()->size();
		
		// our only processing: hex dup the payload to the log
		INF_("[%s]: incoming data:\n%s",c_name(),hex_dump(ptr,len).c_str());
		
		// IMPORTANT: with returning len, read buffer will be truncated by 'len' bytes. Note: truncated bytes are LOST.
		return len;
	};
};

/*
 now let's override baseProxy, and use on_left/right_bytes method!
 this proxy is working with *already accepted* sockets

 basically Proxy class recognizes LEFT and RIGHT side. You can organize those contexts on both sides.
 it's up to you what will do with them, it doesn't have any particular technical meaning; it just 
 follows the principle that you are usually proxying 2 sides (most commonly clients with servers, but 
 left-right is more generic and follows common sense.
*/

class MitmProxy : public baseProxy {
public:	
	explicit MitmProxy(baseCom* c) : baseProxy(c) {};
    
	// this virtual method is called whenever there are new bytes in any LEFT host context!
	virtual void on_left_bytes(baseHostCX* cx) {
		
		// because we have left bytes, let's copy them into all right side sockets!
		for(std::vector<baseHostCX*>::iterator j = right_sockets.begin(); j != right_sockets.end(); j++) {

			// to_read: returns readbuf's buffer "view" of previously processed bytes 
			// to_write: this is appending to caller's write buffer

			// next line therefore calls processing context to return new processed bytes (to_read is like to offer: I have new processed data, read it if you want)
			// those processed data will be wiped by next read() call, so let's now write them all to right socket!
			(*j)->to_write(cx->to_read());
		}
	};
	
	// let's make this one short
    virtual void on_right_bytes(baseHostCX* cx) {
		for(std::vector<baseHostCX*>::iterator j = left_sockets.begin(); j != left_sockets.end(); j++) {
			(*j)->to_write(cx->to_read());
		}
	}
	
	// ... and also when there is error on L/R side, claim the proxy DEAD. When marked dead, it will be safely 
	// closed by it's master proxy next cycle.
    virtual void on_left_error(baseHostCX* cx) { 
		ERR_("Left error: %s",cx->c_name());
		dead(true); 
	};
	
	virtual void on_right_error(baseHostCX* cx) { 
		ERR_("Right error: %s",cx->c_name());
		dead(true); 
	};	
};


/*
 Previous proxy (MyProxy) is copying bytes left to right and vice versa. All right, but how we can manage
 incoming connections? We would need many MyProxy instances and something which will be maintaining them.
 
 For this purpose we have here so called MasterProxy. It's equipped with a vector of child proxies and is
 running them on it's own. 
 
 Master proxy is being used for receiving new connections, whenever new connection is established,
 it's creating new child proxy, attaching just accepted socket to some side of the child proxy. See
 how it's easy to implement something like that.
 */

class MyMasterProxy : public MasterProxy {
    

public:
    MyMasterProxy(baseCom* c) : MasterProxy(c) {};
    
	// override this function to return some specific host context... in our case it's MyHostCX
	// this function is called internally, when accepting new socket on BOUND left/right sockets.
	// BOUND sockets is another vector of sockets specifically maintained for sockets from bind() 
	// 
	// We just learned, that we have in the Proxy classes vectors of:
	//   * common L/R sockets
	//   * bound L/R sockets
	//   * permanent L/R sockets (we haven't spoken about them yet - see next tuturial examples)
	
	// new_cx is allocator of new host context for just accepted L/R socket
	virtual baseHostCX* new_cx(int s) { 
		return new MyHostCX(s); 
	};	
	
    virtual void on_left_new(baseHostCX* just_accepted_cx) {
		// ok, we just accepted socket, created context for it (using new_cx) and we probably need ... 
		// to create child proxy and attach this cx to it.
		
		MitmProxy *new_proxy = new MitmProxy(com()->replicate());
		
// 		just_accepted_cx->unblock();
// 		just_accepted_cx->sslcom_server_fd = just_accepted_cx->socket();
// 		just_accepted_cx->init_server();
		
		// let's add this just_accepted_cx into new_proxy
		new_proxy->ladd(just_accepted_cx);
		
		// now ... we want to connect accepted socket to some other host (target). 
		// let's create new host context for the target and connect to it
		MyHostCX *target_cx = new MyHostCX(target_host.c_str(),target_port.c_str());
		
		// connect it! - btw ... we don't want to block of course...
		target_cx->connect(false);
		
		// almost done, just add this target_cx to right side of new proxy
		new_proxy->radd(target_cx);
		
		
		// FINAL point: adding new child proxy to the list
		proxies().push_back(new_proxy);
	}

public:
	// let's be lazy and make it easy and publicly accessible
	std::string target_host;
	std::string target_port;
};


// Now let's do the Ctrl-C magic
static MyMasterProxy* main_proxy;

void my_terminate (int param)
{
  FATS_("Terminating ...");
  main_proxy->shutdown();
  exit(1);
}

int main(int argc, char *argv[]) {
	
	// setting logging facility level
	lout.level(DIA);
	
	// some idiot-proof help
	if (argc != 4) {
		ERR_("Usage: %s<listen_port> <target_hostname> <target_port>",argv[0]);
		return -1;
	}
	
	MyMasterProxy master = MyMasterProxy(new SSLCom());
	main_proxy = &master;

	// bind with master proxy (.. and create child proxies for new connections)
	if (master.bind(std::stoul(argv[1]),'L') < 0) {
		FATS_("Error binding port, exiting");
		return -1;
	};
	
	// set the target host
	master.target_host = argv[2];
	master.target_port = argv[3];
	
	// install signal handler, we do want to release the memory properly
		// signal handler installation
	void (*prev_fn)(int);
	prev_fn = signal (SIGTERM,my_terminate);
	if (prev_fn==SIG_IGN) signal (SIGTERM,SIG_IGN);

	prev_fn = signal (SIGINT,my_terminate);
	if (prev_fn==SIG_IGN) signal (SIGINT,SIG_IGN);
	
	signal(SIGPIPE, SIG_IGN);
	
	master.run();
	INFS_("Closing");
}