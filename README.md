# Socle README #

Socle stands for SOcket Library Ecosystem. Its is (so far) static library which could be used to simplify operation on sockets and (which is more important
from library philosophy) chaining them with other sockets.
In other words, Socle's main purpose is to make comfortable writing various proxy applications between 2 sockets.

# THIS PROJECT #

This project maintains examples of Socle usage. Either for documentation purposes, or for testing of Socle itself.
The code here won't work without having socle. To make it work, download socle and socle_examples into the same directory level.
Then go to socle_examples and run cmake.
  
* Repository is owned by Ales Stibal <astib@mag0.net>