#include <csignal>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>

#include <logger.hpp>
#include <hostcx.hpp>
#include <baseproxy.hpp>
#include <masterproxy.hpp>
#include <threadedacceptor.hpp>
#include <sslcom.hpp>
#include <display.hpp>

/*
 *  Example of interconnecting two sockets using baseProxy library class, continuing with code 
 *  partially taken from test_connect2.cpp.
 * 
 *  Look for NEW: -- the rest will stay the same as from original source we are continuing from
 */


// STOLEN from test_connect2.cpp - when processing bytes from readbuf, hex-print all data
// inherit from baseHostCX, use TCPCom communication layer

// NEW: 
//      TCPCom is giving you the possiblity to allow accepting non-local sockets. You 
// 		just turn on this option bore call of bind().
//		There is also couple of attributes supporting non-local operation; their names 
//      are prepended with nonlocal_.
// 		For example: nonlocal_host and nonlocal_port are variables which will tell you
// 		where the original session was trying to connect.

//		Otherwise this class is very same, doing the same thing.
//		
//		BEWARE: you cannot run on it on your machine to test it. Whenever you will attempt
//			to connect to local IP address, child proxy will try to connect to the same 
//			IP:PORT you attempted, which is this proxy again .. so loop will occur. 
//			This loop will create chain of child proxies, which will end with depletition
//          of sockets/memory/stack, whichever will comes first.

std::string target_appl;

class MyHostCX : public baseHostCX {
public:
	
	// surprise, constructor filling hostname and port
	MyHostCX( const char* h, const char* p ) : baseHostCX::baseHostCX(new TCPCom(),h,p) {
		DEB_("MyHostCX: constructor %s:%s",h,p);
	};
	MyHostCX( int s ) : baseHostCX::baseHostCX(new TCPCom(),s) {
		DEB_("MyHostCX: constructor %d",s);
	};
	
	// first useful code: overriding process() method: do something with read buffer when bytes arrived into the socket
    virtual int process() {
		
		// IMPORTANT: where are those incoming data? In the readbuf() !!!
		unsigned char *ptr = baseHostCX::readbuf()->data();
		unsigned int len = baseHostCX::readbuf()->size();
		
		// our only processing: hex dup the payload to the log
		DIAS_("Incoming data(" + this->name() + "):\n" +hex_dump(ptr,len));
		
		// IMPORTANT: with returning len, read buffer will be truncated by 'len' bytes. Note: truncated bytes are LOST.
		return len;
	};
};

/*
 now let's override baseProxy, and use on_left/right_bytes method!
 this proxy is working with *already accepted* sockets

 basically Proxy class recognizes LEFT and RIGHT side. You can organize those contexts on both sides.
 it's up to you what will do with them, it doesn't have any particular technical meaning; it just 
 follows the principle that you are usually proxying 2 sides (most commonly clients with servers, but 
 left-right is more generic and follows common sense.
*/

// NEW: Just for the case you are wondering if here is anything new: no, it's not! :)
// 		L<->R copy mechanism stays the same.

class MitmProxy : public baseProxy {
public:	
	explicit MitmProxy(baseCom* c) : baseProxy(c),  
	writing_(false),
	writer_(NULL),
	writer_key_l_("???:???"),
	writer_key_r_("???:???") {
	};
    
	virtual ~MitmProxy() {
		if(writing_) {
			DIAS_("MyProxy::destructor: syncing writer");
			writer_write("Connection stop\n",'L');
			delete writer_;
		}
	}
	
	
	std::string create_writer_key(char side) {
		if (side == 'L' || side == 'l') {
			
			if (this->lsize() > 0) {
				this->host_l_ = this->left_sockets.at(0)->host();
			}
			
			if (this->lsize() > 0 && this->rsize() > 0 ) {
				return string_format("%s-%s",this->left_sockets.at(0)->c_name(),this->right_sockets.at(0)->c_name());
			}
			else if (this->lsize() > 0) {
				return string_format("%s-%s",this->left_sockets.at(0)->c_name(),"unknown");
			} 
			else if (this->lsize() > 0) {
				return string_format("%s-%s","unknown",this->right_sockets.at(0)->c_name());
			} 
			else {
				return std::string("unknown-unknown");
			}
		} else {
			if (this->lsize() > 0 && this->rsize() > 0 ) {
				return string_format("%s-%s",this->right_sockets.at(0)->c_name(),this->left_sockets.at(0)->c_name());
			}
			else if (this->lsize() > 0) {
				return string_format("%s-%s",this->right_sockets.at(0)->c_name(),"unknown");
			} 
			else if (this->lsize() > 0) {
				return string_format("%s-%s","unknown",this->left_sockets.at(0)->c_name());
			} 
			else {
				return std::string("unknown-unknown");
			}
		}
	}
	
	bool create_writer() {
		
		writer_key_l_ = create_writer_key('L');
		writer_key_r_ = create_writer_key('R');
		
		std::string datadir = "mitm/";
		mkdir(datadir.c_str(),700);
		
		
		std::string hostdir = datadir+host_l_+"/";
		mkdir(hostdir.c_str(),0770);

		time_t now = time(0);
		struct tm loc;
		localtime_r(&now,&loc);
		std::string datedir = string_format("%d-%02d-%02d/",loc.tm_year+1900,loc.tm_mon+1,loc.tm_mday);
		mkdir((hostdir+datedir).c_str(),700);
		
		std::string file_datepart = string_format("%02d-%02d-%02d_",loc.tm_hour,loc.tm_min,loc.tm_sec);
		
		writer_ = new std::ofstream(hostdir + datedir + "mitm_dump_" + file_datepart + writer_key_l_);
		if(writer_->is_open()) {
			writing_ = true;
			return true;
		}
		
		writing_ = false;
		delete writer_;
		return false;
	}
	
	void writer_write(buffer b, char side) {
		if (side == 'l' || side == 'L') {
			writer_write(hex_dump(b.data(),b.size(),0,'>'),side);
		} else {
			writer_write(hex_dump(b.data(),b.size(),2,'<'),side);
		}
	}
	
	void writer_write(std::string s, char side) {
		
		timeval now;
		gettimeofday(&now,NULL);
		char d[64];
		memset(d,0,64);
		ctime_r(&now.tv_sec,d);
		
		std::string& k1 = writer_key_l_;
		std::string& k2 = writer_key_r_;
		if (side == 'R' || side == 'r') {
			k1 = writer_key_r_;
			k2 = writer_key_l_;
		}
		
		if(! writing_) {
			if (create_writer()) {
				DIA_("writer '%s' created",writer_key_l_.c_str());
			} else {
				ERR_("write '%s' creation failed!",writer_key_l_.c_str());
			}
		}
		
		if (writing_) {
			
			*writer_ << d << "+" << now.tv_usec << ": "<< k1 << "(" << k2 << ")\n";
			*writer_ << s << '\n';
		}
	}
	
	
	// this virtual method is called whenever there are new bytes in any LEFT host context!
	virtual void on_left_bytes(baseHostCX* cx) {
		
		writer_write(cx->to_read(),'L');
		
		// because we have left bytes, let's copy them into all right side sockets!
		for(typename std::vector<baseHostCX*>::iterator j = this->right_sockets.begin(); j != this->right_sockets.end(); j++) {

			// to_read: returns readbuf's buffer "view" of previously processed bytes 
			// to_write: this is appending to caller's write buffer

			// next line therefore calls processing context to return new processed bytes (to_read is like to offer: I have new processed data, read it if you want)
			// those processed data will be wiped by next read() call, so let's now write them all to right socket!
			(*j)->to_write(cx->to_read());
		}
	};
	
	// let's make this one short
    virtual void on_right_bytes(baseHostCX* cx) {
		writer_write(cx->to_read(),'R');
		for(typename std::vector<baseHostCX*>::iterator j = this->left_sockets.begin(); j != this->left_sockets.end(); j++) {
			(*j)->to_write(cx->to_read());
		}
	}
	
	// ... and also when there is error on L/R side, claim the proxy DEAD. When marked dead, it will be safely 
	// closed by it's master proxy next cycle.
    virtual void on_left_error(baseHostCX* cx) { 
		DIAS_("on_left_error: proxy marked dead");
		writer_write("Client side connection closed: " + cx->name() + "\n",'L');
		
		this->dead(true); 
	};
	
	virtual void on_right_error(baseHostCX* cx) { 
		DIAS_("on_right_error: proxy marked dead");
		writer_write("Server side connection closed: " + cx->name() + "\n",'R');
		
		this->dead(true); 
	};	

private:
	bool writing_;
	std::ofstream *writer_;
	std::string writer_key_l_;
	std::string writer_key_r_;
	std::string host_l_;
};


/*
 Previous proxy (MyProxy) is copying bytes left to right and vice versa. All right, but how we can manage
 incoming connections? We would need many MyProxy instances and something which will be maintaining them.
 
 For this purpose we have here so called MasterProxy. It's equipped with a vector of child proxies and is
 running them on it's own. 
 
 Master proxy is being used for receiving new connections, whenever new connection is established,
 it's creating new child proxy, attaching just accepted socket to some side of the child proxy. See
 how it's easy to implement something like that.
 */


// NEW: Here we are getting to the point: how we will connect to remote peer, if the connection was 
// 		terminated here? Well, TCPCom class is equipped with tools to resolve original destination.
// 		See how we are calling in on_left_new, where changes are apparent there.

class MyMasterProxy : public ThreadedAcceptorProxy<MitmProxy> {
	
public:
    MyMasterProxy(baseCom* c,int i) : ThreadedAcceptorProxy< MitmProxy >(c, i) {};    
    // override this function to return some specific host context... in our case it's MyHostCX
	// this function is called internally, when accepting new socket on BOUND left/right sockets.
	// BOUND sockets is another vector of sockets specifically maintained for sockets from bind() 
	// 
	// We just learned, that we have in the Proxy classes vectors of:
	//   * common L/R sockets
	//   * bound L/R sockets
	//   * permanent L/R sockets (we haven't spoken about them yet - see next tuturial examples)
	
	// new_cx is allocator of new host context for just accepted L/R socket
	virtual baseHostCX* new_cx(int s) { 
		return new MyHostCX(s); 
	};	
	
    virtual void on_left_new(baseHostCX* just_accepted_cx) {
		// ok, we just accepted socket, created context for it (using new_cx) and we probably need ... 
		// to create child proxy and attach this cx to it.
		
		// NEW: whole method is reorganized 

		if(! just_accepted_cx->com()->nonlocal_dst_resolved()) {
			ERRS_("Was not possible to resolve original destination!");
			just_accepted_cx->close();
			delete just_accepted_cx;
		} 
		else {
			MitmProxy *new_proxy = new MitmProxy(com()->replicate());
			
			// let's add this just_accepted_cx into new_proxy			
			new_proxy->ladd(just_accepted_cx);
			MyHostCX *target_cx = new MyHostCX(just_accepted_cx->com()->nonlocal_dst_host().c_str(), 
											   string_format("%d",just_accepted_cx->com()->nonlocal_dst_port()).c_str()
											  );
			// connect it! - btw ... we don't want to block of course...
			target_cx->connect(false);

			//NEW: end of new
			
			// almost done, just add this target_cx to right side of new proxy
			new_proxy->radd(target_cx);
			
			// write start message
			new_proxy->writer_write("Connection start\n",'L');
			// FINAL point: adding new child proxy to the list
			this->proxies().push_back(new_proxy);
		}
	}


};

typedef ThreadedAcceptor<MyMasterProxy,MitmProxy> theAcceptor;
typedef ThreadedAcceptor<MyMasterProxy,MitmProxy> theSSLAcceptor;

// Now let's do the Ctrl-C magic
static Proxy* main_proxy;

void my_terminate (int param)
{
  FATS_("Terminating ...");
  main_proxy->shutdown();
  exit(1);
}

int main(int argc, char *argv[]) {
	
	// setting logging facility level
	lout.level(INF);
	
	// some idiot-proof help
	if (argc != 3) {
		ERR_("Usage: %s <listen_port> <443 = SSL, any value cleartext>",argv[0]);
		return -1;
	}
	
	target_appl = argv[2] ;

	int mode = std::stol(target_appl);

	if( mode == 443) {
		INF_("Entering SSL mode: %d",mode);
		auto p = new theAcceptor(new SSLCom());
		p->com()->nonlocal_dst(true);

		// bind with master proxy (.. and create child proxies for new connections)
		if (p->bind(std::stoul(argv[1]),'L') < 0) {
			FATS_("Error binding port, exiting");
			return -1;
		};

		p->run();
		
		main_proxy = (Proxy*)p;
	} else {
		INF_("Entering plaintext mode: %d",mode);
		auto p = new theAcceptor(new TCPCom());
		p->com()->nonlocal_dst(true);
		// bind with master proxy (.. and create child proxies for new connections)
		if (p->bind(std::stoul(argv[1]),'L') < 0) {
			FATS_("Error binding port, exiting");
			return -1;
		};

		p->run();
		
		main_proxy = (Proxy*)p;
	}
	
	
	
	// install signal handler, we do want to release the memory properly
		// signal handler installation
	void (*prev_fn)(int);
	prev_fn = signal (SIGTERM,my_terminate);
	if (prev_fn==SIG_IGN) signal (SIGTERM,SIG_IGN);

	prev_fn = signal (SIGINT,my_terminate);
	if (prev_fn==SIG_IGN) signal (SIGINT,SIG_IGN);
	
// 	main_proxy->run();

	delete main_proxy;
}